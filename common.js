const tabsTitles = document.querySelectorAll('.tabs-title');
tabsTitles[0].classList.add("active");
const tabsContents = document.querySelectorAll('.tabs-content li');
tabsContents[0].style.display = 'block';

document.addEventListener('click', (e) => {
  const clickedElem = e.target;
  
  if (clickedElem.classList.contains('tabs-title')) {
    const selectedTab = clickedElem.dataset.name;
    
    for (let i = 0; i < tabsContents.length; i++) {
      const content = tabsContents[i];
      
      const contentName = content.dataset.name;
      
      content.style.display = contentName === selectedTab ? 'block' : 'none';
    }
    
    for (let i = 0; i < tabsTitles.length; i++) {
      tabsTitles[i].classList.remove('active');
    }
    
    clickedElem.classList.add('active');
  }
});
